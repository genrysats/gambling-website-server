
const SteamUser = require('steam-user');
const SteamTotp = require('steam-totp');
const SteamCommunity = require('steamcommunity');
const TradeOfferManager = require('steam-tradeoffer-manager');
const config = require('./trade_bot_config.js');

const client = new SteamUser();
const community = new SteamCommunity();

let status = 0; //0 => Inactive, 1 => Active

const manager = new TradeOfferManager({
	steam: client,
	community: community,
	language: 'en'
});

let sentOffers = {}; //id -> {dbcallback, userid, status: (0 = waiting, 1 = not accepted, 2 = accepted), totalPrice: , timeSent}

manager.on('sentOfferChanged', function(offer, oldState) {
	if(offer.state === 3)  //Acceepted 
	{
		if(sentOffers[offer.id]) {
			sentOffers[offer.id].dbcallback(); //Transfer the coins, and adds item to db.
			sentOffers[offer.id].status = 2;
		}
		
	}
	else if(offer.state !== 2){
		if(sentOffers[offer.id])
			sentOffers[offer.id].status = 1;
	}

	if(offer.state !== 2)
		console.log('offer state changed...');
});

const logInOptions = {
	accountName: config.accountName,
	password: config.password,
	twoFactorCode: SteamTotp.generateAuthCode(config.sharedSecret)
};

/*
console.log(logInOptions);
return;*/
client.logOn(logInOptions);


client.on('loggedOn', () => {
	console.log('logged on');
});

client.on('webSession', (sid, cookies) => {
	console.log('web session');

	status = 1;

	manager.setCookies(cookies, function(err) {
		if (err) {
			console.log("set Cookies", err);
			return;
	}});

	community.setCookies(cookies);
	community.startConfirmationChecker(20000, config.identitySecret);

});

setInterval(() => {
	let keys = Object.keys(sentOffers);
	let currTime = Date.now();
	for(let i = 0 ; i < keys ; i++) {
		let key = keys[i];
		if(Math.abs(sentOffers[key].timeSent - currTime) / 1000 / 60) {
			
		}
		Math.round(((diffMs % 86400000) % 3600000) / 60000);
		
	}
}, 10 * 60000); //10 Minutes, clear the trades

																				//receiver: 0 = We are getting the items, 1 = We are sending the items
exports.sendTrade = (steamid, steamtradeurl, data, callback, dbcallback, userid, receiver = 0) => {
	if(status != 1) {
		return callback({'success': false, message: 'Please try again in a few minutes...' });		
	}
	
	if(receiver) {
		steamid = config.steamid;
	}

	manager.getUserInventoryContents(steamid, 730, 2, true, (err, inventory, currencies) => {
		let offer = manager.createOffer(steamtradeurl);
		
		offer.getUserDetails((err, me, them) => {
			//console.log(me);
			//console.log(them);
			if(err || them.escrowDays > 0) {
				return callback({'success': false,'message': 'Our trade will be onhold status, therefore we can not send the trade, please try again later.'});
			}
		});	

		let count = 0;

		inventory.forEach(item => {
			if(data[item.market_hash_name] && data[item.market_hash_name].selected > 0) {
				data[item.market_hash_name].selected--;
				if(data[item.market_hash_name].selected <= 0) {
					delete data[item.market_hash_name];
				}
				
				if(receiver)
					offer.addMyItem(item);
				else
					offer.addTheirItem(item);				
			}

			count++;
		});
		
		if(Object.keys(data).length != 0) {
			return callback({'success': false, 'message': 'Something went wrong...'});
		}

		let flag = false;
		//console.log(offer);
		offer.send((err, status) => {
			if(err) {
				flag = true;
				console.log(err);
				return callback({'success': false, 'message': 'Something went wrong..'});
			}
			sentOffers[offer.id] = { status: 0, dbcallback, userid, timeSent: Date.now(), receiver};
			return callback({'success': true, 'message': 'Trade offer has been sent succesffuly, please accept it.', 'offerid': offer.id});
		})

	});
}

exports.checkStatus = (offerid, userid, callback) => {
	if(!sentOffers[offerid] || sentOffers[offerid].userid !== userid) 
	{
		return callback({'success': false, 'message': 'Wrong offer'});
	}

	if(sentOffers[offerid].status === 2) {
		delete sentOffers[offerid];
		return callback({'success': true, 'message': 'Offer has been accepted'});
	}

	if(sentOffers[offerid].status === 1) {
		delete sentOffers[offerid];
		return callback({'success': true, 'message': 'Offer has been declined'});
	}

	if(sentOffers[offerid].status === 0) {
		return callback({'success': true, 'message': 'Waiting'});
	}

	return callback({'success':false, 'message': 'Something went wrong'});
}