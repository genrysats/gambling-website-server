const StreamObj = require('stream-json/streamers/StreamObject');
const request = require('request')

const db = require("./models/index");
const Item = db.items;

const updateTime = 1.5 * 3600 * 1000; //1.5 hours in miliseconds

const getData = async () => {
    console.log('Updating table..');

    let queries = [];
    let promises = [];

    const jsonStream = StreamObj.withParser();

    jsonStream.on('data', ({key, value}) => {
        queries.push({ market_hash_name: key, safe_price: value});

        if(queries.length >= 900) {
            promises.push(Item.bulkCreate([...queries]));
            queries = [];
        }
        
    });

    jsonStream.on('end', () => {
        Promise.all(promises).then(() => {
            console.log('Update is over');
            db.sequelize.query('SET FOREIGN_KEY_CHECKS = 1', { raw: true });;
        });    
    });
    
    await db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', null, { raw: true });
    Item.destroy({ truncate: true, cascade: false}).then(() => {
        console.log('Cleared table items');
        request({url: 'https://api.steamapis.com/market/items/730?format=compact&api_key=H4II3-DOIPTZcSoCAC_cbPyjpiQ'}).pipe(jsonStream);
    });
    
}

const items = require("./controllers/item.controller");

items.updateTime((time) => {
    console.log(`Time to wait: ${new Date(time * 1000).toISOString().substr(11, 8)} till next item update`);

    if(time <= 0) {
        getData();
        setInterval(getData, updateTime);
    }else {
        setTimeout(() => {
            getData();
            setInterval(getData, updateTime);
        }, time * 1000)
    }

});