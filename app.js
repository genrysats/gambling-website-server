var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/uesrs');
var slotsRouter = require('./routes/slots');
var pokerRouter = require('./routes/poker');
var depositRouter = require('./routes/deposit');
var withdrawRouter = require('./routes/withdraw');

require('./updateItemTable.js');

var bot = require('./trade_bot.js');

//setTimeout(bot.sendTrade, 3000);

var cors = require('cors');
var app = express();
app.locals.bot = bot;

app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/slots', slotsRouter);
app.use('/poker', pokerRouter);
app.use('/deposit', depositRouter);
app.use('/withdraw', withdrawRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//Globals
global.dollarInCoins = 500; //1.5 Hours, in seconds.
global.minDollar = 0.02; //Min 2 dollar deposit.
global.timeToWait = 1.5 * 3600; //1.5 Hours, in seconds.
global.commission = 0.1;

const db = require("./models/index");
db.sequelize.sync();



module.exports = app;
