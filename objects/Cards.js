const PokerEvaluator = require("poker-evaluator");

class Cards {
    constructor() {
        this.cards = [];

        this.suitToColor = {'Clubs': 0, 'Diamonds': 1, 'Spades': 0, 'Hearts': 1}; //black = 0, red = 1
        this.valueConvert = {11: 'J', 12: 'Q', 13: 'K', 14: 'A'};
    }

    generate() {
        this.cards = [];

        let card = (value, suit) => {
            this.value = value;
            this.suit = suit;
            this.color = this.suitToColor[suit];

            this.name = (value > 10 ? this.valueConvert[value] : value) + suit.charAt(0);
            this.name = value == 10 ? 'T' + suit.charAt(0) : this.name;
            return {value: this.value, suit: this.suit, color: this.color, name: this.name};
        }
        
        let values = [2,3,4,5,6,7,8,9,10,11,12,13,14] //11 = J, 12 = Q, 13 = K, 14 = A
        let suits = ['Clubs', 'Diamonds', 'Spades', 'Hearts'];

        for(let i = 0 ; i < values.length; i++){
            for(let j = 0 ; j < suits.length; j++){
                this.cards.push(card(values[i], suits[j]));
            }
        } 

        this.shuffle();
    }

    evalHand(all_cards) {
        return PokerEvaluator.evalHand(all_cards);
    }

    shuffle() {
        let counter = this.cards.length;

        // While there are elements in the array
        while (counter > 0) {
            // Pick a random index
            let index = Math.floor(Math.random() * counter);

            // Decrease counter by 1
            counter--;

            // And swap the last element with it
            let temp = this.cards[counter];
            this.cards[counter] = this.cards[index];
            this.cards[index] = temp;
        }
    }

    deal(howMany) { 
        let cards = [];
        for(let i = 0; i < howMany ; i++) {
            cards.push(this.cards.shift());
        }

        return cards;
    }

    dealCards() {
        return this.deal(2);
    }

    flop() {
        return this.deal(3);
    }

    turn() {
        return this.deal(1);
    }
    
    river() {
        return this.deal(1);
    }

}

module.exports = Cards;
