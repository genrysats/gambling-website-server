const users = require('../controllers/user.controller');

class Room {
    constructor(maxPlayers, creator, roomName, dbUsers, minBet) {
        this._bigBlind = 0;
        this._smallBlind = 1;
        this._players = {}; //user.email => {user, socket, coins, turn: 0/1, cards, won: true/false}
        this._room_id = require('crypto').randomBytes(30).toString('hex');

        this._dbUsers = dbUsers;

        this._current_min_bet = minBet;
        this._MIN_BET = minBet;

        this._lastAction = Date.now();

        this._coinsOnTable = 0;
        this.current_turn = -1;
        this.max_curr_turn = 0;

        this._cards = null;
        this._cardsOnTable = [];

        this._currState = 0; //0 = Not Started, 1 = Players have to take action, 2 = Game Ended
        this._action = 0; //0 = No Action, 1 = Flop, 2 = Turn, 3 = River

        this._maxPlayers = maxPlayers;
        this._creator = creator;
        this._roomName = roomName;

        this.timeout = null;
    }
    
    get playersCount() {
        return Object.keys(this._players).length;
    }

    get lastAction() {
        return this._lastAction;
    }
    setLastAction() {
        console.log('upadting last action time...');
        this._lastAction = Date.now();
    }

    checkRoomFull() {
        return this._players.length < this._maxPlayers;
    }

    setAction(action)
    {
        this._action = action;
    }

    setCurrState(state) {
        this._currState = state;
    }

    get minBet() { 
        return this._MIN_BET;
    }

    get action() {
        return this._action;
    }
    
    get coinsOnTable() {
        return this._coinsOnTable;
    }

    get cards() {
        return this._cards;
    }

    get creator() {
        return this._creator;
    }
    get roomName() {
        return this._roomName;
    }
    get room_id() {
        return this._room_id;
    }
    
    get players() {
        return this._players;
    }
    
    get currState() {
        return this._currState;
    }

    get current_min_bet() {
        return this._current_min_bet;
    }

    checkWhoWon() {
        let tableCards = Object.values(this._cardsOnTable).map(item => item.name);
        let max_value = 0;
        let won_players = [];
        let won_desc = '';
        
        for(let id in this._players) {
            if(this._players[id].active) {
                let hand = this._cards.evalHand([...tableCards, ...this._players[id].cards.map(item => item.name)]);
                if(max_value == 0 || hand.value > max_value) {
                    max_value = hand.value;
                    won_players = [];
                    won_players.push(this._players[id]);

                    won_desc = hand.handName;
                }else if(hand.value == max_value) {
                    won_players.push(this._players[id]);
                    won_desc = hand.handName;
                }
            }
        }


        let coins = this._coinsOnTable / won_players.length;
        let count = 0;

        for(let playerWon in won_players) {
            let id = won_players[playerWon].user.id;
            this._players[id].won = true;

            this._dbUsers.updateCoins(id, coins, (obj) => {
                if(count == won_players.length - 1) {
                    this.sendStateToAll(true, won_desc);
                }
                count++;
            });
        }

    }

    checkGameEnded() {
        let active = [];
        for(let playerID in this._players) {
            if(this._players[playerID].active) {
                active.push(this._players[playerID]);
            }
        }

        if(active.length <= 1) {
            this._currState = 2;
        }else {
            return null; //The game is not ended, and nobody won yet.
        }

        if(active.length == 1) {
            return active[0]; //The player who won the game, because all the others is not active.
        }

        return null; //Strange situation when the game ended, but nobody won.
    }

    setCurrentMinBet(bet) {
        this._current_min_bet = bet;
    }
    
    initTurn() {
        let i = 0;
        for(let playerID in this._players) {
            if(i == 0) this._players[playerID].turn = 1;
            else this._players[playerID].turn = 0;
            i++;
        }

        this._current_min_bet = this._MIN_BET;
        this.current_turn = 0;
        this.max_curr_turn = Object.values(this._players).filter(item => item.active).length; //Last ACTIVE player
    }

    setPlayerInactive(playerID) {
        if(!playerID || !this._players[playerID]) return;
        this._players[playerID].active = false;

        let flag = this._players[playerID].turn == 1; //If it was his turn, move the turn forward.
        this._players[playerID].turn = 0;
        
        this._players[playerID].cards = [];
        


        if(flag) {
            
        }

    }
    restartGame() {
        setTimeout( () => {
            console.log('starting the game again...');
            this._action = 0;
            let count = 0;

            //TODO later on: run over the players who WANTED another game.
            for(let id in this._players) {
                if(count == 0) {
                    this._players[id].turn = 1;
                }else{
                    this._players[id].turn = 0;
                }
                count++;
                this._players[id].active = true;
            }

            this.max_curr_turn = Object.keys(this._players).length;
            this._cardsOnTable = [];
            this.setLastAction();
            this.startGame();
        }, 2000); 
    }

    startAction() {
        let dealt_cards = null;
        this.initTurn();
        this.initPlayersCoins();

        if(this._action == 1) {
            dealt_cards = this._cards.flop();
        }else if(this._action == 2) {
            dealt_cards = this._cards.turn();
        }else if(this._action == 3) {
            dealt_cards = this._cards.river();
        }else {
            this._currState = 2;
            //End Game
            this.checkWhoWon();
            this.restartGame();

            console.log('end game');
            return;
        }

        this._cardsOnTable.push(...dealt_cards);
        this._current_min_bet = 0;

        this.sendStateToAll();
    }

    sendStateToAll(lastState = false, won_desc = null) { //lastState means if this is the last update for this current game.
        let length = Object.keys(this._players).length - 1;
        let i = 0;
        for(let id in this._players) {
            this._dbUsers.getCoins(id, (ans) => {
                if(!this._players[id] || !this._players[id].socket) return;
              if(!ans.success) {this._players[id].socket.emit('newAction', JSON.stringify({})); console.log(ans);}
              else {
                this._players[id].socket.emit('newAction', JSON.stringify({...this.getGameState(id, ans.coins), won_desc}));
              }
              if(lastState && i == length){
                  this._currState = 0;
              }
              i++;
            });
        }
    }

    turnTimeout() {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            let ids = Object.keys(this._players);

            console.log('Next turn...');

            console.log(`${this._players[ids[this.current_turn % ids.length]].user.id} missed his turn..` );
            let user = this._players[ids[this.current_turn % ids.length]].user;

            
            this.setPlayerInactive(user.id);

            //Check if game ended
            let won = this.checkGameEnded();
            if(won && this.currState == 2) {
              console.log(`game ended`);
              console.log(`player won: ${won.user.id}`);
              users.updateCoins(won.user.id, this.coinsOnTable, (obj) => {
                  this.sendStateToAll();
                  this.restartGame();
              });
            }else{
                this.nextTurn(false);
                this.sendStateToAll();
            }

        }, 1000 * 30); //30 Seconds

    }

    startGame(cards = null) {
        console.log(`starting game..current turn: ${this.current_turn}, max turn: ${this.max_curr_turn}, `);
        this._current_min_bet = this._MIN_BET;
        this._currState = 0;

        if(cards) this._cards = cards;
        
        this._cards.generate();
    
        for(let playerID in this._players) {
            let player = this._players[playerID];
            let dealt_cards = this._cards.dealCards();
            player.cards = dealt_cards;
            
            player.socket.emit('StartGame', {cards: dealt_cards, state: this.getGameState(player.user.id)});
        }
        
        this.turnTimeout();

        this._currState = 1;
    }

    endGame() {
        this._currState = 2;
        this._action = 0;
    }

    initPlayersCoins() {
        for(let playerID in this._players) {
            this._players[playerID].coins = 0;
        }
    }

    getGameState(playerID, coins = null) {   
        return {
            players: Object.values(this._players).map((player) => {
                return {
                    username: player.user.username, 
                    coinsOnTable: player.coins, //coinsOnTable(of a single player)
                    turn: player.turn,
                    me: player.user.id == playerID,
                    coins: coins != null && player.user.id == playerID ? coins : null,
                    active: player.active,
                    won: player.won,
                    cards: this._currState == 2 && player.active ? player.cards : null
                }
            }),
            current_min_bet: this.current_min_bet,
            gameState: this._currState,
            action: this._action,
            cards: this._cardsOnTable,
            coinsOnTable: this._coinsOnTable
        };
    }

    addCoins(coins, playerID) {
        this._players[playerID].coins += coins;
        this._coinsOnTable += coins;
    }

    getPlayersCount() {
        return Object.keys(this._players).length;
    }

    addPlayer(user, socket) {
        let first = false;
        let len = Object.keys(this._players).length;
        

        if(len == 0) {
            this.current_turn = 0;
            this.max_curr_turn = 0;
            first = true;
        }

        if(this._players[user.id])
            return false; //Already exists.

        if(len > 1) //Other new players have to wait untill the game starts again
        {
            this._players[user.id] = {user: user, socket: socket, coins: 0, turn: 0, active: false, won: false};
        }else {
            this._players[user.id] = {user: user, socket: socket, coins: 0, turn: first ? 1 : 0, active: true, won: false};
            this.max_curr_turn = len + 1;
        }
        
        return true;
    }

    removePlayer(id) {
        delete this._players[id];
    }

    checkActive(playerID) {
        if(!playerID || !this._players[playerID]) return false;
        return this._players[playerID].active;
    }
    
    nextTurn(raise = false) {
        let ids = Object.keys(this._players);
        this._players[ids[this.current_turn % ids.length]].turn = 0;

        this.turnTimeout();
        
        if(raise && this.current_turn > 0) {
            this.max_curr_turn = ids.length + this.current_turn - 1;
        }

        let flag = false;

        console.log(`current: ${this.current_turn}, max: ${this.max_curr_turn}`);

        if((raise && this.current_turn >= this.max_curr_turn) || (!raise && this.current_turn >= this.max_curr_turn - 1)) {
            console.log(`current: ${this.current_turn}, max: ${raise ? this.max_curr_turn : this.max_curr_turn-1}`);
            flag = true;
            this._action++;
        }else{
            console.log(this.current_turn);
            console.log( this.max_curr_turn);
        }

        if(!flag) {
            let found = false;
            console.log(`curr turn: ${this.current_turn}`);
            //Find next ACTIVE player turn
            for(let i = this.current_turn + 1 ; i <= this.max_curr_turn ; i++) {
                console.log(`for, i: ${i}, index: ${this.current_turn % ids.length}`);
                if(this._players[ids[i % ids.length]].active) {
                    this.current_turn = i;
                    found = true;
                    break;
                }
            }
            
            if(!found) {
                console.log('not found active player..');
                return;
            }
            console.log(`next active player is ${this.current_turn}`);

        }
        console.log(`max curr turn: ${this.max_curr_turn}`);

        if(!flag){
            console.log(`setting turn 1 to player: ${this._players[ids[this.current_turn % ids.length]].user.id}`)
            this._players[ids[this.current_turn % ids.length]].turn = 1;
        }
        else 
            this._players[ids[this.current_turn % this.max_curr_turn]].turn = 1;

        if(flag) {
            this.startAction();
        }

        return flag;

    }

    getPlayerTurn() {
        if(this.current_turn == -1) return null;
        let ids = Object.keys(this._players);
        return this._players[ids[this.current_turn % ids.length]];
    }
};

module.exports = Room;
