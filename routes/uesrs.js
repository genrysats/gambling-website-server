var express = require('express');
var router = express.Router();

const users = require("../controllers/user.controller");
const jwt = require('jsonwebtoken');

const passport  = require('passport');
const SteamStrategy = require('passport-steam');

passport.use(new SteamStrategy({
    returnURL: 'http://localhost:3000/users/steam_login',
    realm: 'http://localhost:3000/',
    apiKey: '7CAE868F83A6CF803EF9653925C50B44'
  },
  function(identifier, profile, done) {
      console.log(profile);
      return done(null, profile);
  }
));

let validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};


function verifyToken(req, res, next) {
    const token = req.headers['authorization'];

    if(token) {
        jwt.verify(token, 'secretkey', (err, authData) => {
            if(err) return res.json({'success': false, message: 'You have to log in..', login: 1});
            req.authData = authData;
            next();
        });
    }else {
        res.json({'success': false, message: 'You have to log in..', login: 1});
    }
}

router.post('/update_tradeurl', verifyToken, (req, res) => {
    if(!req.body.tradeurl) {
        res.json({'success': false, 'message': 'Missing trade url'});
    }
    let re = new RegExp('https:\\/\\/steamcommunity.com\\/tradeoffer\\/new\\/\\?partner=[A-Za-z0-9]+&token=[A-Za-z0-9]+$');

    if (!re.test(req.body.tradeurl)) {
        res.json({'success': false, 'message': 'Invalid trade url'});
        return;
    }
    users.updateTradeUrl(req.authData.user.id, req.body.tradeurl, (ans) => {
        res.json(ans);
    });
    
    
    return;
});

router.post('/login', function(req, res, next) {
    if(!req.body.email || !validateEmail(req.body.email))
        res.json({'success': false, 'message': 'Missing email'});
    if(!req.body.password || req.body.password.length < 5 || req.body.password.length > 20)
        res.json({'success': false, 'message': 'Missing password'});

    users.login(req.body.email, req.body.password, (ans, user=null) => {
        if(!user)
            return res.json(ans);
        
        jwt.sign( { exp: Math.floor(Date.now() / 1000) + (60 * 60), user }, 'secretkey', (err, token) => {
            if(err) {
                res.json({'success': false, 'message': 'error...'});
            }
            res.json( {...ans, token, username: user.username, avatar: user.avatar, steamid: user.steamid } );
        });

    });
});

router.get('/steam_login', (req, res) => {
    passport.authenticate('steam',  (err, user, info) => {      
        if(err) {
            console.log(err);
            return res.json({success: false, error: err});
        }
        
        //Check user data if exists

        users.continueSteam(user._json.avatarmedium, user.displayName, user.id, null, null, null, (ans, user=null) => {
            if(!user)
                return res.json(ans);

            jwt.sign( { exp: Math.floor(Date.now() / 1000) + (60 * 60), user }, 'secretkey', (err, token) => {
                if(err)
                    res.json({'success': false, 'message': 'error...'});
                
                res.send("<script> window.opener.steam('" + JSON.stringify({...ans, token, username: user.username, avatar: user.avatar, steamid: user.steamid }) + "'); window.close();</script>");
            });
        });

    })(req, res);
    
});


router.post('/getCoins', verifyToken, (req,res,next) => {
    users.getCoins(req.authData.user.id, (ans) => {
        res.json(ans);
    })
});

router.post('/register', function(req,res,next) {
    if(!req.body.email || !validateEmail(req.body.email))
        res.json({'success': false, 'message': 'Missing email'});
    if(!req.body.password || req.body.password.length < 5 || req.body.password.length > 20)
        res.json({'success': false, 'message': 'Missing password'});
    if(!req.body.username || req.body.username.length < 4)
        res.json({'success': false, 'message': 'Missing username'});
    
    users.register(req.body.email, req.body.password, req.body.username, (ans, user=null) => {
        if(!user)
            return res.json(ans);
            
        jwt.sign( { exp: Math.floor(Date.now() / 1000) + (60 * 60), user }, 'secretkey', (err, token) => {
            if(err) {
                res.json({'success': false, 'message': 'error...'});
            }
            console.log({...ans, token});

            res.json( {...ans, token, username: user.username} );
        });
        
    });
})



module.exports = router;
