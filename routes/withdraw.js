var express = require('express');
var router = express.Router();

const items = require("../controllers/item.controller");
const users = require("../controllers/user.controller");
const botItems = require('../controllers/bot_item.controller');

const jwt = require('jsonwebtoken');

const SYSTEM_EMAIL = 'systemuser@user.com';

function verifyToken(req, res, next) {
    const token = req.headers['authorization'];

    if(token) {
        jwt.verify(token, 'secretkey', (err, authData) => {
            if(err) return res.json({'success': false, message: 'You have to log in..', login: 1});
            req.authData = authData;
            next();
        });
    }else {
        res.json({'success': false, message: 'You have to log in..', login: 1});
    }
}

router.post('/', verifyToken, (req, res) => {
    if(!req.body.inv) {
        return res.json({'success': false, 'message': 'Items missing'});
    }

    if(!req.authData.user.steamtradeurl) {
        return res.json({'success': false, 'message': 'Please connect a steam acocunt...'});
    }

    if(!req.authData.user.steamtradeurl) {
        return res.json({'success': false, 'message': 'TradeURL is missing, please update.'});
    }
    
    let inv = req.body.inv;
    let keys = Object.keys(inv);

    if(!Array.isArray(keys) || keys.length > 500 || keys.length === 0) {
        return res.json({'success': false, 'message': 'Something went wrong, please try again later..'});
    }

    let names = keys;

    items.calculatePrice(names, (prices) => {
        let totalPrice = 0;

        for(let i in keys) {
            let hash_name = keys[i];
            if(!prices[hash_name]) {
                res.json({'success': false, 'message': `Invalid item "${hash_name}", min deposit value is 2$.`});
                return;
            }
            totalPrice += prices[hash_name] * inv[hash_name].selected;
        }
        
        let commission = totalPrice * global.commission;
        totalPrice = Math.floor(totalPrice - commission);
        
        req.app.locals.bot.sendTrade(req.authData.user.steamid, req.authData.user.steamtradeurl, JSON.parse(JSON.stringify(inv)), (ans) => {
            res.json({...ans, totalPrice});
        }, () => {
            botItems.addBotItems(names,JSON.parse(JSON.stringify(inv)), (ans1) => {
                if(ans1.success) {
                    users.updateCoins(null, commission, (system_res) => {
                        users.updateCoins(req.authData.user.id, totalPrice, (ans2) => {
                            console.log('updated user coins');
                            //res.json(ans1);
                        });
                        
                    }, SYSTEM_EMAIL);
                    
                }else {
                    //res.json(ans1);
                    console.log(ans1);
                }
            });
            
        }, req.authData.user.id);
    });

    
  //res.json({'success': true});
});

router.post('/checkStatus', verifyToken, (req, res) => {
    if(!req.body.offerid) {
        return res.json({'success': false, 'message': 'Missing offer..'});
    }

    req.app.locals.bot.checkStatus(req.body.offerid, req.authData.user.id, (ans) => {
        res.json(ans);
    });
    
});


//Get user inventory with prices
router.post('/inv', (req, res) => {
    if(!req.body.inv) {
        return res.json({'success': false, 'message': 'Items missing'});
    }

    let inv = req.body.inv;
   
    if(!Array.isArray(inv) || inv.length > 500 || inv.length === 0) {
        return res.json({'success': false, 'message': 'Something went wrong, please try again later..'});
    }

    let names = inv.map(a => a.hash_name);
    let ans = [];

    items.calculatePrice(names, (prices) => {
        inv.forEach(element => {
            if(prices[element.hash_name]) {
                ans.push({...element, price: prices[element.hash_name] });
            }
        });
        res.json({'success': true, 'inv': ans})
    });
});

module.exports = router;
