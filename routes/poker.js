var express = require('express');
var router = express.Router();

const users = require("../controllers/user.controller");
const jwt = require('jsonwebtoken');

const Room = require("../objects/Room");


function verifyToken(req, res, next) {
    const token = req.headers['authorization'];

    if(token) {
        jwt.verify(token, 'secretkey', (err, authData) => {
            if(err) return res.json({'success': false, message: 'You have to log in..', login: 1});
            req.authData = authData;
            next();
        });
    }else {
        res.json({'success': false, message: 'You have to log in..', login: 1});
    }
}

let count = 0; //For debug

router.post('/new_room', verifyToken, (req, res, next) => {
    const minBetArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    if(!req.body.minBet || isNaN(req.body.minBet*1) || !minBetArr.includes(req.body.minBet*1) ) {
        res.json({'success': false, message:'Min bet is missing'});
        return;
    }

    let minBet = req.body.minBet*1;
    
    for(let id in global.rooms) {
        if(global.rooms[id].minBet == minBet && global.rooms[id].checkRoomFull) {
            res.json({'success': true, room_id: id });
            return;
        }
    }
    
    let room = new Room(5, 'me', 'name', users, minBet);
    global.rooms[room.room_id] = room;
    res.json({'success': true, room_id: room.room_id });
    
});


module.exports = router;
