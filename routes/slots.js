var express = require('express');
var router = express.Router();

const users = require("../controllers/user.controller");
const jwt = require('jsonwebtoken');

function randomNumber(min=1, max=9) {
    return Math.floor(Math.random() * (max - min) + min);
}

function verifyToken(req, res, next) {
    const token = req.headers['authorization'];

    if(token) {
        jwt.verify(token, 'secretkey', (err, authData) => {
            if(err) return res.json({'success': false, message: 'You have to log in..', login: 1});
            req.authData = authData;
            next();
        });
    }else {
        res.json({'success': false, message: 'You have to log in..', login: 1});
    }
}




var last_wins = []; //Holds last 10 wins

router.get('/last_wins', (req, res, next) => {
    res.json({'success': true, last_wins});
});

router.post('/submit', verifyToken, (req,res,next) => {
    //TO DO: Check logged in

    if(!req.body.bet) {
        return res.json({'success': false, message:'Missing bet value'});
    }
    let bet = req.body.bet;
        
    if(!Number.isInteger(bet) || bet < 100 || bet > 2000) {
        return res.json({'success': false, message:'Bad bet..'});
    }


    let callback = (ans) => {
        if(ans.success) {
            if(ans.win) last_wins.push(ans.added);
            if(last_wins.length > 10) last_wins.shift();

            res.json({ ...ans, data: {first, second, third}, last_wins, bet: bet });
        }else {
            res.json({ ...ans, bet: bet});
        }
    };

    let first = randomNumber(), second = randomNumber(), third = randomNumber();


    if(first == second && first == third){
        users.updateCoins(req.authData.user.id, (bet * (first+1) * 30), callback);
    }else {
        //Sub user's coins by 300
        users.updateCoins(req.authData.user.id, -bet, callback);
    }
});

module.exports = router;
