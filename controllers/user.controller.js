const db = require("../models/index");
const bcrypt = require('bcrypt');

const User = db.users;
const saltRounds = 10;

const REGISTER_COINS = 1000;

exports.continueSteam = (avatar, username, steamid, coins=null, email=null, password=null, callback) => {

    const user = {
        email: email,
        password: password,
        coins: coins === null ? REGISTER_COINS : coins,
        username,
        avatar,
        steam: 1,
        steamid
    };

    db.sequelize.transaction((t) => {
        return User.findOrCreate({
          where: {
            steamid
          },
          defaults: user,
          transaction: t
        })
        .spread((userResult, created) => {
            let notification = '';

            if(!userResult.steamtradeurl || userResult.steamtradeurl == '')
                notification = 'Please add your public URL trade';             

          if (created) {
            return callback({ success: true, message: 'User created successfully', notification }, user);
          }
          
          if(userResult.dataValues.username == username && userResult.dataValues.avatar == avatar) {
            return callback({ success: true, message: 'Successfully logged in', notification }, userResult.dataValues);
          }

          //Update username & avatar
          userResult.update({ avatar, username }).then( (ans) => {
            return callback({ success: true, message: 'Successfully logged in', notification }, ans.dataValues);
          });
        });
    });

};

exports.register = async (email, password, username, callback, avatar = 'empty') => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
        if(err) return callback({ success: false, message:'Something wrong with this password' });

        const user = {
            email: email,
            password: hash,
            coins: REGISTER_COINS,
            username: username,
            avatar: avatar,
            steam: 0
        };
        
        User.create(user).then((data) => {
            let notification = '';

            if(data.steam == 0) {
                notification = 'Please add your steam account(Click the profile picture)';
            }else if(!data.steamtradeurl || data.steamtradeurl == ''){
                notification = 'Please add your public URL trade';     
            }           
            
            return callback({ success: true, message: 'User created successfully', notification}, user);
        }).catch(db.Sequelize.UniqueConstraintError, (err) => {
            return callback({ success: false, message: 'This email already exists'});
        }).catch(err => {
            console.log(err);
            return callback({ success: false, message: 'Something went wrong, try again later'});
        });

    });
}

exports.login = (email, password, callback) => {
    User.findOne({
        where: {
            email: email
        }
    }).then(data => {
        if(bcrypt.compare(password, data.password, function(err, match) {
            if(err || !match) return callback({ success: false, message: 'Wrong Credentials'});
            
            let notification = '';

            if(data.steam == 0) {
                notification = 'Please add your steam account(Click the profile picture)';
            }else if(!data.steamtradeurl || data.steamtradeurl == ''){
                notification = 'Please add your public URL trade';                
            }

            return callback({ success: true, message: 'Successfully logged in', notification }, data);
        }));
    }).catch( () => { return callback({ success: false, message: 'Wrong Credentials' }); });
};

exports.getCoins = (id, callback) => {
    User.findOne( {where: {id}}).then( user => {
        return callback({ success: true, coins: user.coins});
    }).catch((err) => {
        return callback({ success: false, message: 'Wrong Credentials' });
    });
};

exports.updateCoins = (id=null, coinsToAdd, callback, email=null) => {
    let tmp = { 'id': id};

    if(!id) {
        tmp = { 'email': email };
    }
    User.findOne({ where: tmp }).then( user => {
        if(user) {
            if(user.coins + coinsToAdd < 0) return callback({ success: false, message: 'Not enough coins'});

            user.update({ coins: user.coins + coinsToAdd }).then( () => {
                return callback({ success: true, win: coinsToAdd > 0, coins: user.coins, added: coinsToAdd });
            }).catch( (err) => {
                console.log(err);
                return callback({success: false, message: 'Cant do that'});
            });
        }else {
            return callback({success: false, message: 'Wrong Credentials'});
        }
    }).catch( (err) => {
        return callback({success: false, message: 'Wrong Credentials'});
    })
}

exports.updateTradeUrl = (id, tradeurl, callback) => {
    User.findOne({ where: {id} }).then( user => {
        if(user) {

            user.update({ steamtradeurl: tradeurl }).then( () => {
                return callback({ success: true, message: 'Updated succesfully' });
            }).catch( (err) => {
                return callback({success: false, message: 'Cant do that'});
            });
        }else {
            return callback({success: false, message: 'Wrong Credentials'});
        }
    });
};