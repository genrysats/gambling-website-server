const db = require("../models/index");
const e = require("express");

const Op = require("sequelize").Op;

const Item = db.items;

/* Global variables defined in app.js */

exports.updateTime = (callback) => { //Returns how much time left untill the next update
    Item.findAll({ limit: 1 }).then((items) => {
        if(items.length == 0) {
            return callback(-1); //No need to wait, because there's no data
        }

        let item = items[0];
        let time = ((Date.now() - new Date(item.updatedAt)) / 1000); //Seconds

        return callback(global.timeToWait - time);
        
    });
};

exports.calculatePrice = (market_hash_names, callback) => { //market_hash_names = array of hash names
    let prices = {}; //Hash name -> price

    Item.findAll({ where: { market_hash_name: {
        [Op.or]: [...market_hash_names]
    } } }).then( items => {
        
        items.forEach(obj => {
            if(!obj.safe_price || parseFloat(obj.safe_price) < global.minDollar){
                prices[obj.market_hash_name] = null;
            }else {
                prices[obj.market_hash_name] = Math.floor(parseFloat(obj.safe_price) * global.dollarInCoins);
            }
        });

        return callback(prices);
    } );
};