const db = require("../models/index");

const BotItem = db.botItems;
const Item = db.items;


exports.getAllItems = (start=0 , callback) => { //Start = start from where.. 
    BotItem.findAll({ offset: start, limit: 100, attributes: ['id', 'updatedAt'], include:[
        {
            model: Item, 
            //as:'users',
            attributes: ['safe_price', 'market_hash_name'],
            //include: [Item.safe_price]
        }]}).then((items) => {
            for(let i in items) {
                let item = items[i];
                //item.price = 
            }
        return callback({'success': true, 'data': items});
    });
};

exports.addBotItems = async (market_hash_names, inv, callback) => {
    let err = 0; 
    console.log(inv);
    err = await market_hash_names.forEach(async hash_name => {
        let tmp = await BotItem.findOrCreate({
              where: {
                itemMarketHashName: hash_name
              },
              defaults: { // set the default properties if it doesn't exist
                itemMarketHashName: hash_name,
                count: inv[hash_name].selected
              }
            }).spread((item, created) => {

              if (!created) { // false if author already exists and was not created.
                item.update({count: item.count + inv[hash_name].selected}).catch(() => { return 1; });
              }
        });
        if(tmp === 1) return tmp;
    });

    if(err === 1) {
        return callback({'success': false, 'message': 'Unknown error..please contact us'});
    }else {
        return callback({'success': true, 'message': 'Items has been added.'});
    }
}
