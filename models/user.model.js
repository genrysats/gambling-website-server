module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true,
      },
      password: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      coins: {
        type: Sequelize.BIGINT,
        allowNull: false,
      },
      steam: {
        type: Sequelize.CHAR,
        allowNull: false,
        defaultValue: 0
      },
      avatar: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      steamid: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true,
      },
      steamtradeurl: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true
      }
    });
  
    return User;
};