module.exports = (sequelize, Sequelize) => {
    const botItem = sequelize.define("botItem", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        count: {
            type: Sequelize.INTEGER,
            defaultValue: 1,
            allowNull: false
        },
        //Later on add a bot field(who has the item)
    });
  
    return botItem;
};