const dbConfig = require("../config/db.config");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.db, dbConfig.user, dbConfig.password, {
    host: dbConfig.host,
    dialect: 'mysql',
    logging: false
});
sequelize.options.logging = false


const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require('./user.model')(sequelize, Sequelize);
db.items = require('./item.model')(sequelize, Sequelize);
db.botItems = require('./bot_item.model')(sequelize, Sequelize);

db.botItems.belongsTo(db.items);

module.exports = db;
