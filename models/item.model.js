module.exports = (sequelize, Sequelize) => {
    const Item = sequelize.define("item", {
      market_hash_name: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      safe_price: {
        type: Sequelize.DECIMAL,
        allowNull: false,
      },
    });
  
    return Item;
};